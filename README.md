# watchClipboard
A module that uses Electron's Clipboard API to watch the clipboard

Should work using raw node.js but idk its just a module i quickly put together


## Installation
### From NPM
```npm i watchclipboard```
### From Source
*Note that this method requires TypeScript installed, although it is a dependency, so it should install it anyway*
```bash
git clone https://gitlab.com/0J3/watchclipboard.js.git
cd watchclipboard.js.git
npm install
npm run build
```

## Usage
1. Load the module using 
> ```typescript
> const clipboard = require(`watchclipboard`)
> ```
> (or)
> ```typescript
> const {
>    watchChanges
>    unWatchChanges
>} = require(`watchclipboard`)
>```
2. Start watching clipboard changes using
> ```typescript
> clipboard.watchChanges((clipboardText: string)=>{
>    // code here
> })
> ```
> *remove :string for it to work in js. this example is for typescript*

## Notice
Since this uses an interval to check for changes, expect higher cpu usage and minor delays from when the clipboard changes, to when the event fires.<br>
If the callback errors, it will likely spam the console.
